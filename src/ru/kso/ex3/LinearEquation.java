package ru.kso.ex3;

import java.util.Scanner;

public class LinearEquation {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите x, y, z");
        double x = scanner.nextDouble();
        if (x <= 1 || x >= -1) {
            double y = scanner.nextDouble();
            double z = scanner.nextDouble();
            double gamma = 5 * Math.atan(x) - (0.25 * Math.acos(x) *
                    ((x + 3 * Math.abs(x - y) + Math.pow(x, 2)) /
                            (Math.abs(x - y) * z + Math.pow(x, 2))));
            System.out.printf("%.4f", gamma);
        } else {
            System.out.println("x должен быть в пределах от -1 до 1");
        }
    }
}
