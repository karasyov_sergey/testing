package ru.kso.ex3;

import java.util.Scanner;

public class BranchingAlgorithm {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Выберите функцию f(x):\n1 - cos(x)\n2 - x^2\n3 - e^x");
        int var = scanner.nextInt();
        System.out.println("Введите x, y");
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();
        switch (var) {
            case 1:
                if (x * y < 5) {
                    System.out.println(Math.pow(Math.cos(x), 3) + Math.sin(y));
                } else if (x * y > 7) {
                    System.out.println(Math.cosh(Math.pow(Math.cos(x), 3)) + Math.pow(y, 2));
                } else {
                    System.out.println(Math.cos(x + Math.pow(Math.cos(x), 3)));
                }
                break;
            case 2:
                if (x * y < 5) {
                    System.out.println(Math.pow(x, 6) + Math.sin(y));
                } else if (x * y > 7) {
                    System.out.println(Math.cosh(Math.pow(x, 6)) + Math.pow(y, 2));
                } else {
                    System.out.println(Math.cos(x + Math.pow(x, 6)));
                }
                break;
            case 3:
                if (x * y < 5) {
                    System.out.println(Math.pow(Math.exp(x), 3) + Math.sin(y));
                } else if (x * y > 7) {
                    System.out.println(Math.cosh(Math.pow(Math.exp(x), 3)) + Math.pow(y, 2));
                } else {
                    System.out.println(Math.cos(x + Math.pow(Math.exp(x), 3)));
                }
                break;
            default:
                System.out.println("Такого варианта выбора функции не существует");
        }
    }
}
