package ru.kso.ex2.triangle;

class Triangle {
    private int a;
    private int b;
    private int c;

    Triangle(int a, int b, int c) {
        if (isTrueSides(a, b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new IllegalArgumentException();
        }
    }


    private boolean isTrueSides(int a, int b, int c) {
        return a > 0 && b > 0 && c > 0;
    }

    boolean isIsoscelesTriangle() {
        return a == b || a == c || b == c;
    }

    boolean isEquilateralTriangle() {
        return a == b && a == c;
    }

    boolean isQuadTriangle() {
        int indexMaxSide = getIndexMaxFrom(a, b, c);
        boolean bool = false;
        switch (indexMaxSide) {
            case 0:
                bool = (b * b + c * c == a * a);
                break;
            case 1:
                bool = (a * a + c * c == b * b);
                break;
            case 2:
                bool = (a * a + b * b == c * c);
                break;

        }
        return bool;
    }

    private int getIndexMaxFrom(int... args) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < args.length; i++) {
            if (args[i] > max) {
                max = args[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }
}
