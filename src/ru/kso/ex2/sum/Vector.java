package ru.kso.ex2.sum;

class Vector {

    private Point pointStart;
    private Point pointEnd;

    Vector(Point pointStart, Point pointEnd) {
        if (isParallelToAxis(pointStart, pointEnd)) {
            this.pointStart = pointStart;
            this.pointEnd = pointEnd;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isParallelToAxis(Point pointStart, Point pointEnd) {
        return pointStart.getY() == pointEnd.getY();
    }

    Point getPointStart() {
        return pointStart;
    }

    Point getPointEnd() {
        return pointEnd;
    }
}
