package ru.kso.ex2.sum;

import java.util.Scanner;

public class Sum {

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Vector[] vectors = new Vector[5];
        for (int i = 0; i < vectors.length; i++) {
            vectors[i] = new Vector(new Point(scanner.nextInt(), scanner.nextInt()),
                    new Point(scanner.nextInt(), scanner.nextInt()));
        }
        int maxX1 = getMaxX(vectors[0], vectors[1], vectors[2]);
        int maxX2 = getMaxX(vectors[3], vectors[4]);
        int minX1 = getMinX(vectors[0], vectors[1], vectors[2]);
        int minX2 = getMinX(vectors[3], vectors[4]);
        if (maxX1 <= minX2) {
            Vector shadowOne = new Vector(new Point(minX1, 0),
                    new Point(maxX1, 0));
            Vector shadowTwo = new Vector(new Point(minX2, 0),
                    new Point(maxX2, 0));
            double sumOne = getVectorLen(shadowOne);
            double sumTwo = getVectorLen(shadowTwo);
            double sum = sumOne + sumTwo;
            System.out.println(sum);
        } else {
            System.out.println("Не соблюден промежуток между отрезками");
        }
    }

    private static double getVectorLen(Vector vector) {
        double temp;
        int startX = vector.getPointStart().getX();
        int startY = vector.getPointStart().getY();
        int endX = vector.getPointEnd().getX();
        int endY = vector.getPointEnd().getY();
        temp = Math.sqrt(Math.pow((endX - startX), 2) + Math.pow((endY - startY), 2));
        return temp;
    }

    private static int getMinX(Vector... vectors) {
        int minX = Integer.MAX_VALUE;
        int tempStart;
        int tempEnd;
        for (Vector vector : vectors) {
            tempStart = vector.getPointStart().getX();
            tempEnd = vector.getPointEnd().getX();
            if (tempStart < minX) {
                minX = tempStart;
            }
            if (tempEnd < minX) {
                minX = tempEnd;
            }
        }
        return minX;
    }

    private static int getMaxX(Vector... vectors) {
        int maxX = Integer.MIN_VALUE;
        int tempStart;
        int tempEnd;
        for (Vector vector : vectors) {
            tempStart = vector.getPointStart().getX();
            tempEnd = vector.getPointEnd().getX();
            if (tempStart > maxX) {
                maxX = tempStart;
            }
            if (tempEnd > maxX) {
                maxX = tempEnd;
            }
        }
        return maxX;
    }
}
