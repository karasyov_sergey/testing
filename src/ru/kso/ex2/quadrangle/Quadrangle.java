package ru.kso.ex2.quadrangle;

import ru.kso.ex2.sum.Point;

class Quadrangle {

    private Point highPoint1;
    private Point highPoint2;
    private Point lowPoint1;
    private Point lowPoint2;

    Quadrangle(Point highPoint1, Point highPoint2, Point lowPoint1, Point lowPoint2) {
        this.highPoint1 = highPoint1;
        this.highPoint2 = highPoint2;
        this.lowPoint1 = lowPoint1;
        this.lowPoint2 = lowPoint2;
    }

    boolean isQuad() {
        double[] sideLen = calcSideLen();
        return sideLen[0] == sideLen[1] && sideLen[0] == sideLen[2] && sideLen[0] == sideLen[3] &&
                highPoint1.getX() == lowPoint1.getX() && lowPoint2.getX() == highPoint2.getX();
    }

    boolean isParallelogram() {
        double[] sideLen = calcSideLen();

        return sideLen[0] == sideLen[1] && sideLen[2] == sideLen[3] &&
                highPoint1.getY() == highPoint2.getY() && lowPoint1.getY() == lowPoint2.getY() &&
                highPoint1.getX() != lowPoint1.getX() && highPoint2.getX() != lowPoint2.getX();
    }

    boolean isSquare() {
        double[] sideLen = calcSideLen();
        return sideLen[0] == sideLen[1] && sideLen[2] == sideLen[3] &&
                highPoint1.getX() == lowPoint1.getX() && lowPoint2.getX() == highPoint2.getX() &&
                sideLen[0] != sideLen[2];
    }

    boolean isRhombus() {
        double[] sideLen = calcSideLen();
        return sideLen[0] == sideLen[1] && sideLen[0] == sideLen[2] && sideLen[0] == sideLen[3] &&
                highPoint2.getX() == lowPoint1.getX() && highPoint1.getY() == lowPoint2.getY();
    }

    boolean isIsoscelesTrapezoid() {
        double[] sideLen = calcSideLen();
        return sideLen[2] == sideLen[3] && highPoint1.getY() == highPoint2.getY() &&
                lowPoint1.getY() == lowPoint2.getY() && sideLen[0] != sideLen[1];

    }

    private double[] calcSideLen() {
        double[] temp = new double[4];
        temp[0] = Math.sqrt(Math.pow(highPoint2.getX() - highPoint1.getX(), 2) +
                Math.pow(highPoint2.getY() - highPoint1.getY(), 2));
        temp[1] = Math.sqrt(Math.pow(lowPoint1.getX() - lowPoint2.getX(), 2) +
                Math.pow(lowPoint1.getY() - lowPoint2.getY(), 2));
        temp[2] = Math.sqrt(Math.pow(highPoint2.getX() - lowPoint2.getX(), 2) +
                Math.pow(highPoint2.getY() - lowPoint2.getY(), 2));
        temp[3] = Math.sqrt(Math.pow(highPoint1.getX() - lowPoint1.getX(), 2) +
                Math.pow(highPoint1.getY() - lowPoint1.getY(), 2));
        return temp;
    }

    boolean isQuadTrapezoid() {
        double[] sideLen = calcSideLen();
        return (highPoint1.getX() == lowPoint1.getX() && highPoint2.getX() != lowPoint1.getX() ||
                highPoint1.getX() != lowPoint1.getX() && highPoint2.getX() == lowPoint1.getX()) &&
                sideLen[0] != sideLen[1];
    }

    boolean isTrapezoid() {
        double[] sideLen = calcSideLen();
        return sideLen[0] != sideLen[1] && sideLen[2] != sideLen[3]
                && highPoint1.getY() == highPoint2.getY() && lowPoint1.getY() == lowPoint2.getY();
    }

}
