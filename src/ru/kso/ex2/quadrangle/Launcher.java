package ru.kso.ex2.quadrangle;

import ru.kso.ex2.sum.Point;

import java.util.Scanner;


public class Launcher {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Quadrangle quadrangle = new Quadrangle(new Point(scanner.nextInt(), scanner.nextInt()),
                new Point(scanner.nextInt(), scanner.nextInt()),
                new Point(scanner.nextInt(), scanner.nextInt()),
                new Point(scanner.nextInt(), scanner.nextInt()));
        if (quadrangle.isQuad()) {
            System.out.println("Квадрат");
        } else if (quadrangle.isParallelogram()) {
            System.out.println("Параллелограмм");
        } else if (quadrangle.isSquare()) {
            System.out.println("Прямоугольник");
        } else if (quadrangle.isRhombus()) {
            System.out.println("Ромб");
        } else if (quadrangle.isIsoscelesTrapezoid()) {
            System.out.println("Равнобедренная трапеция");
        } else if (quadrangle.isQuadTrapezoid()) {
            System.out.println("Прямоугольная трапеция");
        } else if (quadrangle.isTrapezoid()) {
            System.out.println("Трапеция общего типа");
        } else {
            System.out.println("Четырехугольник общего типа");
        }
    }
}
