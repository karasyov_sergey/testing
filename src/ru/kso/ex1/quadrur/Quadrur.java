package ru.kso.ex1.quadrur;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Quadrur {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            if (a != 0) {
                double discriminant = Math.pow(b, 2) - 4 * a * c;
                printResults(a, b, discriminant);
            } else {
                System.out.println("Введенное уравнение неквадратное");
            }
        } catch (InputMismatchException e) {
            System.out.println("Введены некорректные данные");
        }
    }

    private static void printResults(double a, double b, double discriminant) {
        if (discriminant < 0) {
            System.out.println("Нет корней");
        } else if (discriminant == 0) {
            double x = -b / 2 * a;
            System.out.println("Корень уравнения = " + x);
        } else {
            double x1 = (-b + Math.sqrt(discriminant)) / 2 * a;
            double x2 = (-b - Math.sqrt(discriminant)) / 2 * a;
            System.out.printf("Первый корень = %.3f%n" +
                    "Второй корень = %.3f%n", x1, x2);
        }
    }
}
