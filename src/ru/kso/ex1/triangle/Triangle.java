package ru.kso.ex1.triangle;

class Triangle {
    private int a;
    private int b;
    private int c;

    Triangle(int a, int b, int c) {
        if (isTrueSides(a, b, c) && isTrueTriangle(a, b, c)) {
            this.a = a;
            this.b = b;
            this.c = c;
        } else {
            throw new IllegalArgumentException();
        }
    }

    private boolean isTrueTriangle(int a, int b, int c) {
        if (isEquilateralTriangle(a,b,c)) {
            return true;
        }
        boolean isTrueTriangle;
        int maxIndex = getIndexMaxFrom(a, b, c);
        switch (maxIndex) {
            case 0:
                isTrueTriangle = b + c < a;
                break;
            case 1:
                isTrueTriangle = a + c < b;
                break;
            case 2:
                isTrueTriangle = a + b < c;
                break;
            default:
                isTrueTriangle = false;
        }

        return isTrueTriangle;
    }

    private int getIndexMaxFrom(int... args) {
        int max = Integer.MIN_VALUE;
        int maxIndex = -1;
        for (int i = 0; i < args.length; i++) {
            if (args[i] > max) {
                max = args[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    private boolean isTrueSides(int a, int b, int c) {
        return a > 0 && b > 0 && c > 0;
    }

    boolean isIsoscelesTriangle() {
        return a == b || a == c || b == c;
    }

    private boolean isEquilateralTriangle(int a,int b, int c) {
        return a == b && a == c;
    }

    boolean isEquilateralTriangle(){
        return a == b && a == c;
    }
}
