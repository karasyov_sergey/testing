package ru.kso.ex1.triangle;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TriangleLauncher {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            int firstSide = scanner.nextInt();
            int secondSide = scanner.nextInt();
            int thirdSide = scanner.nextInt();
            Triangle triangle = new Triangle(firstSide, secondSide, thirdSide);
            if (triangle.isEquilateralTriangle()){
                System.out.println("Треугольник равносторонний");
            } else if (triangle.isIsoscelesTriangle()){
                System.out.println("Треугольник равнобедренный");
            } else {
                System.out.println("Треугольник неравносторонний");
            }
        } catch (IllegalArgumentException | InputMismatchException e) {
            System.out.println("Неверно введены данные" +
                    " или слишком большие значения");
        }
    }


}
