package ru.kso.ex4;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class HeapSortTest {

    private int[] numbersOne;
    private int[] numbersTwo;
    private int[] numbersThree;
    private int[] numbersOneSort;
    private int[] numbersTwoSort;
    private int[] numbersThreeSort;

    @Before
    public void prepareData() {
        numbersOne = new int[]{-5, -7, -2, -6, -1, -8, -9, -3, -4};
        numbersTwo = new int[]{9, 1, 2, 8, 5, 6, 4, 3, 7};
        numbersThree = new int[]{11, 19, 10, 15, 14, 16, 17, 12, 18, 13};
        numbersOneSort = new int[]{-9, -8, -7, -6, -5, -4, -3, -2, -1};
        numbersTwoSort = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        numbersThreeSort = new int[]{10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
    }


    @Test
    public void testSortOne() {
        HeapSort.sort(numbersOne);
        for (int i = 0; i < numbersOne.length; i++) {
            assertEquals(numbersOne[i], numbersOneSort[i]);
        }
    }

    @Test
    public void testSortTwo() {
        HeapSort.sort(numbersTwo);
        for (int i = 0; i < numbersTwo.length; i++) {
            assertEquals(numbersTwo[i], numbersTwoSort[i]);
        }
    }

    @Test
    public void testSortThree() {
        HeapSort.sort(numbersThree);
        for (int i = 0; i < numbersThree.length; i++) {
            assertEquals(numbersThree[i], numbersThreeSort[i]);
        }
    }
}